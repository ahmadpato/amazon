require 'selenium-webdriver'
require 'rubygems'
require 'rspec'

options = Selenium::WebDriver::Chrome::Options.new
options.add_argument('--start-maximized')
driver = Selenium::WebDriver.for :chrome, options: options

Given(/^Given I Navigate to amazon homepage$/) do
  driver.navigate.to "https://www.amazon.com/"
  sleep(2)
end

Given(/^Given I Click to login button$/) do
  driver.find_element(:xpath=>'//*[@id="nav-belt"]/ul/li[1]/span/span').click
  sleep(2)
end

Given(/^Given I enter email$/) do
  driver.find_element(:xpath=>'//*[@id="a-page"]/div[1]/input').send_keys('your email')
  sleep(2)
end

Given(/^Given I Click to signup link$/) do
  driver.find_element(:xpath=>'//*[@id="nav-belt"]/ul/li[2]/span/span').click
  sleep(2)
end

Given(/^Given I enter name$/) do
  driver.find_element(:xpath=>'//*[@id="ap_register_form"]/div[1]/input').send_keys('your name')
  sleep(2)
end

Given(/^Given I enter email$/) do
  driver.find_element(:xpath=>'//*[@id="ap_register_form"]/div[2]/input').send_keys('your email')
  sleep(2)
end

Given(/^Given I enter password$/) do
  driver.find_element(:xpath=>'//*[@id="ap_register_form"]/div[3]/input').send_keys('your password')
  sleep(2)
end

Given(/^Given I enter re-enter password$/) do
  driver.find_element(:xpath=>'//*[@id="ap_register_form"]/div[3]/input').send_keys('your re-enter-password')
  sleep(2)
end

Given(/^Given I Click to create amazon account button$/) do
  driver.find_element(:xpath=>'//*[@id="ap_register_form"]/ul/li[1]/span/span').click
  sleep(2)
end
